[[ $0 != $BASH_SOURCE ]] && AUX=$BASH_SOURCE || AUX=$0

PATH_SCRIPT="$( cd "$(dirname "$AUX")" ; pwd -P )"
cd $PATH_SCRIPT || exit


IMAGE_VERSION=$(mvn --non-recursive help:evaluate -Dexpression=project.version | grep -v '\[.*' |tail -1)
LX_VERSION=$IMAGE_VERSION
echo $LX_VERSION

cd - &>/dev/null
