<?php

namespace Illuminate\Tests\Integration\Database;

use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use LaravelPdoOdbc\ODBCConnector;
use Orchestra\Testbench\TestCase;

abstract class DatabaseTestCase extends TestCase
{
    use DatabaseMigrations;

    /**
     * The current database driver.
     *
     * @return string
     */
    protected $driver;

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function defineEnvironment($app)
    {
        self::addLeanXConnection($app);
    }

    public static function addLeanXConnection($app)
    {
        Connection::resolverFor('leanxcale-odbc', ODBCConnector::registerDriver());

        // Setup default database to use leanxcale.
        $app['config']->set('database.default', 'leanxcale-odbc');
        $app['config']->set('database.connections.leanxcale-odbc', [
            'driver' => 'leanxcale-odbc',
            'database' => 'test',
            'dsn' => 'lxodbcdsn',
            'username' => 'test',
            'password' => 'test',
        ]);
    }

    protected function setUp(): void
    {
        $this->beforeApplicationDestroyed(function () {
            foreach (array_keys($this->app['db']->getConnections()) as $name) {
                $this->app['db']->purge($name);
            }
        });
        parent::setUp();

//        /** @var DatabaseManager $db */
//        $db = $this->app['db'];
//        foreach (array_keys($db->getConnections()) as $name) {
////            $db->connection($name)->getSchemaBuilder()->dropAllTables();
//            $db->connection($name)->enableQueryLog();
//        }
    }

    protected function tearDown(): void
    {
        /** @var DatabaseManager $db */
        $db = $this->app['db'];
        foreach (array_keys($db->getConnections()) as $name) {
//            dump("----- connection ------ $name ----- ");
//            dump($db->connection($name)->getQueryLog());
            $db->connection($name)->disconnect();
        }
        parent::tearDown();
    }

    protected function getEnvironmentSetUp($app)
    {
        $connection = $app['config']->get('database.default');

        $this->driver = $app['config']->get("database.connections.$connection.driver");
    }
}
