<?php

namespace LaravelPdoOdbc\Database\Eloquent;

trait UsingLeanBuilder
{
    public function newEloquentBuilder($builder) {
        return new LeanBuilder($builder);
    }

    /**
     * Qualify the given column name by the model's table.
     *
     * @param  string  $column
     * @return string
     */
    public function qualifyColumn($column)
    {
        return ($column);
        if (str_contains($column, '.')) {
            return $column;
        }

        return $this->getTable().'.'.$column;
    }
}
