<?php

namespace LaravelPdoOdbc\Database\Query\Grammars;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Support\Facades\DB;
use function _PHPStan_9a6ded56a\RingCentral\Psr7\parse_request;

class LeanQueryGrammar extends Grammar {

    /**
     * Compile an aggregated select clause.
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $aggregate
     * @return string
     */
    protected function compileAggregate(Builder $query, $aggregate) {

        $column = $this->columnize($aggregate['columns']);

        // If the query has a "distinct" constraint and we're not asking for all columns
        // we need to prepend "distinct" onto the column name so that the query takes
        // it into account when it performs the aggregating operations on the data.
        if (is_array($query->distinct)) {
            $column = 'distinct ' . $this->columnize($query->distinct);
        } else if ($query->distinct && $column !== '*') {
            $column = 'distinct ' . $column;
        }

        // Lean aggregate column name should be escaped.
        return 'select ' . $aggregate['function'] . '(' . $column . ') as "aggregate"';
    }

    /**
     * Compile an insert and get ID statement into SQL.
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $values
     * @param string $sequence
     * @return string
     */
    public function compileInsertGetId(Builder $query, $values, $sequence) {

        return $this->compileInsert($query, $values) . " RETURNINGSEQ";
    }

    /**
     * Compile a date based where clause.
     * @param string $type
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $where
     * @return string
     */
    protected function dateBasedWhere($type, Builder $query, $where) {

        $value = $this->parameter($where['value']);

        // LeanXcale function for DAY().
        if ($type === 'day') {
            $type = 'DAYOFMONTH';
        }
        //        if($type === 'date') {
        // TODO
        //select * from posts where (year(created_at) = '2018' and month(created_at)='01' and DAYOFMONTH(created_at)='02')
        //select * from posts where created_at < TIMESTAMP '2018-01-03';
        //            $date = Carbon::parse($value);
        //            dd($date);
        //        }

        return $type . '(' . $this->wrap($where['column']) . ') ' . $where['operator'] . ' ' . $value;
    }

    /**
     * Compile an insert statement into SQL.
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $values
     * @return string
     */
    public function compileInsert(Builder $query, array $values) {
        // LeanX doesn't support any combinations of default insert,
        // so we're using internal _uuid_ column and uuid() function.
        if (empty($values)) {
            $values = [['_uuid_' => DB::raw('uuid()')]];
        }
        return parent::compileInsert($query, $values);
    }
    /**
     * Compile a basic where clause.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @param  array  $where
     * @return string
     */
    protected function whereBasic(Builder $query, $where)
    {
        // Fixing LeanXcale Bang equal '!=' is not allowed under the current SQL conformance level
        if($where['operator'] === '!=') {
            $where['operator'] = '<>';
        }
        $value = $this->parameter($where['value']);

        $operator = str_replace('?', '??', $where['operator']);

        return $this->wrap($where['column']).' '.$operator.' '.$value;
    }
}
