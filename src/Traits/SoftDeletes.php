<?php

namespace LaravelPdoOdbc\Traits;

use Illuminate\Database\Eloquent\Builder;
use LaravelPdoOdbc\Database\Eloquent\LeanBuilder;
use LaravelPdoOdbc\Database\Eloquent\UsingLeanBuilder;

/**
 * @method static \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder withTrashed(bool $withTrashed = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder withoutTrashed()
 */
trait SoftDeletes {

    use \Illuminate\Database\Eloquent\SoftDeletes;
    use UsingLeanBuilder;
//    public function newEloquentBuilder($builder) {
//
//        return new LeanBuilder($builder);
//    }
}
