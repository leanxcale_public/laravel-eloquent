#!/bin/bash
set -eu
version=$(./lx-version.sh)

## Download from artifactory
echo "Publishing php and connectors..."
echo "lx_version:${version}"
nexus_url='https://nexus.leanxcale.com/service/rest/v1/components?repository='
echo "NEXUS:${nexus_url}"


## Convert X.X-RC.X to X.XrcX
if [[ $version =~ ([0-9]+.[0-9])+-RC.([0-9]+) ]];
then
 normalized_version="${BASH_REMATCH[1]}rc${BASH_REMATCH[2]}"
else
 normalized_version=$version
fi

raw_publish_url="${nexus_url}leanxcale-releases"


mkdir tmp

echo "CREACION..."

cd tmp

echo "DENTRO..."
ls
# Get latest sequelize package
art_url="https://artifactory.leanxcale.com/artifactory/ext-release-local/com/leanxcale/laravel-eloquent"
curl -u ${ARTIFACTORY_USERNAME}:${ARTIFACTORY_SECRET} -L -O $art_url/laravel-eloquent-lx-${version}\_latest.tar

echo "Download artifactory..."

ls

echo "VARIABLES..."
echo ${ARTIFACTORY_USERNAME}
echo ${ARTIFACTORY_SECRET} 
echo ${raw_publish_url}
echo ${normalized_version}


curl -v -u ${AUTOMATE_PUBLIC_ARTIFACTORY_USERNAME}:${AUTOMATE_PUBLIC_ARTIFACTORY_API_KEY} -X POST $raw_publish_url \
-F raw.directory=/laravel-eloquent/${normalized_version} -F raw.asset1=@laravel-eloquent-lx-${normalized_version}_latest.tar -F raw.asset1.filename=laravel-eloquent-lx-${normalized_version}_latest.tar

cd ..
rm -R tmp
# untar -xzf laravel-eloquent-lx-*.tar


