#!/bin/bash
BASEPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#OLDPWD=$(pwd)
DATE=$(date '+%Y-%m-%d-%H:%M:%S')
LXVERSION=$(./lx-version.sh)
mkdir tmp
tar cvf -r $BASEPATH/tmp/laravel-eloquent-lx-${LXVERSION}.tar $BASEPATH
cd $BASEPATH/tmp/
curl -L -u $ARTIFACTORY_USERNAME:$ARTIFACTORY_SECRET -T  laravel-eloquent-lx-*.zip $ARTIFACTORY_URL/laravel-eloquent-lx-${LXVERSION}\_latest.tar
curl -L -u $ARTIFACTORY_USERNAME:$ARTIFACTORY_SECRET -T  laravel-eloquent-lx-*.zip $ARTIFACTORY_URL/laravel-eloquent-lx-${LXVERSION}\_${DATE}.tar
rm -r $BASEPATH/tmp/
cd $OLDPWD
