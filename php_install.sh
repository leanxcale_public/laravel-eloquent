apt update ;
apt upgrade -y
apt install ca-certificates apt-transport-https software-properties-common;
add-apt-repository ppa:ondrej/php -y
apt install php8.1 libapache2-mod-php8.1 -y
cd /usr/bin/
sudo apt-get install  php8.1-odbc
sudo apt-get install -y php8.1-mbstring
sudo apt-get install -y php8.1-dom php8.1-curl
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
sudo mv composer.phar /usr/local/bin/composer