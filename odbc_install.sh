/lx-version.sh > file1
read LX_VERSION < file1
echo $LX_VERSION
cd libs
apt-get update
apt-get install -y unixodbc unixodbc-dev
apt install -y libprotobuf-c-dev
curl -u ${ARTIFACTORY_USERNAME}:${ARTIFACTORY_SECRET} -L -O http://artifactory.leanxcale.com/artifactory/ext-release-local/com/leanxcale/calcite-odbc-driver/lxodbc-${LX_VERSION}_latest.tar.gz
tar -xvf lxodbc-${LX_VERSION}_latest.tar.gz
apt-get purge --auto-remove
apt-get clean
sudo cp odbcinst.ini /etc/odbcinst.ini
sudo cp odbc.ini /etc/odbc.ini
source conf.sh
odbcinst -j
odbcinst -i -d -f odbcinst.ini 
odbcinst -i -s -l -f odbc.ini
odbcinst -s -q [lxodbcdsn]
